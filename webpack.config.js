// const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresetEnv = require('postcss-preset-env');
const { sass } = require('svelte-preprocess-sass');
const path = require('path');

const mode = process.env.NODE_ENV || 'development';
const prod = mode === 'production';

module.exports = {
	entry: {
		bundle: ['./src/main.js'],
		style: './src/main.scss'
	},
	resolve: {
		alias: {
			svelte: path.resolve('node_modules', 'svelte')
		},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	output: {
		path: __dirname + '/dist',
		filename: '[name].js',
		chunkFilename: '[name].[id].js'
	},
	module: {
		rules: [
			{
				test: /\.svelte$/,
				use: {
					loader: 'svelte-loader',
					options: {
						emitCss: true,
						hotReload: true,
						preprocess: {
							style: sass({}, { name: 'scss' }),
						}
					}
				}
			},
			{
                test: /\.s?css$/,
                exclude: /(node_modules|bower_components)/,
                use: [{
                    loader : MiniCssExtractPlugin.loader,
                    options: {
                        // publicPath: '../../../',
                    },
                },{
                    loader: "css-loader",
                },{
                    loader: 'postcss-loader',
                    options: {
                        ident: 'postcss',
                        plugins: [
                            postcssPresetEnv({autoprefixer: {grid: true}})
                        ],
                    },
                },{
                    loader: 'sass-loader'
                }]
            }
		]
	},
	mode,
	plugins: [
		// new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: '[name].css'
		})
	],
	devtool: prod ? false: 'source-map'
};
