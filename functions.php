<?php
	

	function stencil_enqueue_styles() {

		$deps = [];
		wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/dist/style.css', $deps );
	    wp_enqueue_style( 'components', get_stylesheet_directory_uri() . '/dist/bundle.css', $deps );
	    $deps = ['wp-data'];
	    wp_enqueue_script( 'svelte-template', get_stylesheet_directory_uri(). '/dist/bundle.js', $deps, false, true );

	}
	add_action( 'wp_enqueue_scripts', 'stencil_enqueue_styles' );
	

	//========================================================================
	//! Theme Options
	//========================================================================
	
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	 
	if( !function_exists( 'stencil_template_setup' ) ) {
		
		function stencil_template_setup() {
			/*
			 * Make theme available for translation.
			 * Translations can be filed in the /languages/ directory.
			 */
			load_theme_textdomain( 'svelte-template', get_template_directory() . '/languages' );
		
			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );
		
			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );
		
			register_nav_menu( 'menu-top', __('Top Menu' ) );
			register_nav_menu( 'footer', __('Footer') );
		
			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );
			
		}
		
	}
		
	add_action( 'after_setup_theme', 'stencil_template_setup' );
	
	
	
	// Add Theme Support for the following features
	add_theme_support('menus');
	add_theme_support('post-thumbnails');
	add_theme_support( 'title-tag' );

	// Enable Editors to edit Menus
	$role_object = get_role( 'editor' );
	$role_object->add_cap( 'edit_theme_options' );			
	
	
	//========================================================================
	//! Woocommerce
	//========================================================================
	
	// Declare WooCommerce Compatibility and enable features you want to use
	//
	add_action( 'after_setup_theme', function() {
		add_theme_support( 'woocommerce' );
		//add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		//add_theme_support( 'wc-product-gallery-slider' );
	} );
	
	// function testing_jwt() {
	// 	$wp_request_headers = array(
	// 		'Authorization' => 'Basic ' . base64_encode( 'username:password' )
	// 	);
	// 	$args = [
	// 		'body' => [
	// 			'username' => 'admin',
	// 			'password' => 'passw0rd13579'
	// 		],
	// 		'header' => $wp_request_headers
	// 	];
	// 	$url = get_rest_url(null, '/jwt-auth/v1/token');
	// 	$url = get_rest_url(null, '/wp/v2/posts');
	// 	$url = get_rest_url(null, '');
	// 	$url = esc_url_raw( rest_url());
	// 	var_dump($url);
	// 	$response = wp_remote_get($url, $args);
	// 	var_dump($response);
	// 	die();
	// }
	// add_action('after_setup_theme', 'testing_jwt');

	// add_action('http_api_curl', function( $handle ){
	// 	//Don't verify SSL certs
	// 	curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
	// 	curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
	//  }, 10);